package com.fcul.adro.indoorNavigationSystem.services.point_of_interest.room;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import com.fcul.adro.indoorNavigationSystem.repository.RoomRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
@POIRoomServiceBean
public class POIRoomServiceImpl implements RoomService {

  private RoomRepository roomRepository;

  public POIRoomServiceImpl(RoomRepository roomRepository) {
    this.roomRepository = roomRepository;
  }

  @Override
  public List<Room> fetchRooms(PointOfInterest pointOfInterest) {
    return this.roomRepository.findRoomsByPointOfInterestId(pointOfInterest.getIdentifier());
  }
}

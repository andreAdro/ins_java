package com.fcul.adro.indoorNavigationSystem.repository;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PointRepository extends JpaRepository<Point, UUID> {

  /**
   * returns points within the requested range of the provided latitude and longitude the points
   * will be ordered by close to further
   *
   * @param latitude the latitude to lookpup points
   * @param longitude the longitude to lookpup points
   * @param range the range to lookpup points
   * @return a collection of points
   */
  @Procedure(procedureName = "geoDistance")
  List<Point> getPointsByDistance(
      @Param("sourceLatitude") double latitude,
      @Param("sourceLongitude") double longitude,
      @Param("bufferSize") double range
  );
}

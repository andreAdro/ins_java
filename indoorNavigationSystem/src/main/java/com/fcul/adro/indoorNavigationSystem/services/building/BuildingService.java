package com.fcul.adro.indoorNavigationSystem.services.building;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import java.util.List;
import java.util.UUID;

public interface BuildingService {

  List<Building> fetchBuildings();

  UUID createBuilding(Building building);

  Building fetchBuildingById(UUID id);

  void updateBuilding(UUID id, Building building);

  void deleteBuilding(UUID id);
}

package com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry;

import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.GeometryDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.PointDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.GeometryType;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polyline;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class PolylineMapper extends PointMapper {

  protected Polyline geoJsonToPolyline(List<PointDTO> pointsSegment, GeometryType type) {
    List<Point> points = new LinkedList<>();
    for (PointDTO wallCoordinate : pointsSegment) {
      Point point = super.geoJsonCoordinateToPoint(wallCoordinate);
      points.add(point);
    }
    Polyline polyline = new Polyline();
    polyline.setPoints(points);
    polyline.setGeometryType(type);
    return polyline;
  }

  protected List<Double[]> polylineToGeoJsonCoordinates(Polyline polyline) {
    List<Double[]> parsedCoordinates = new ArrayList<>();
    for (Point point : polyline.getPoints()) {
      Double[] coordinate = super.pointToGeoJsonCoordinates(point);
      parsedCoordinates.add(coordinate);
    }
    return parsedCoordinates;
  }

  public FeatureObjectDTO pointsToGeoJsonPolyline(List<Point> points) {
    Polyline polyline = new Polyline();
    polyline.setGeometryType(GeometryType.LINE_STRING);
    polyline.setPoints(points);
    return this.polylineToGeoJson(polyline);
  }

  public FeatureObjectDTO polylineToGeoJson(Polyline building) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinates(building, featureObjectDTO);
    extractOtherData(building, featureObjectDTO);
    return featureObjectDTO;
  }

  private void extractCoordinates(Polyline polyline, FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = polylineToGeoJsonCoordinates(polyline);
    featureObjectDTO.getGeometry().setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry().setType(polyline.getGeometryType());
  }

  private void extractOtherData(Polyline polyline, FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(polyline.getIdentifier());
  }

}

package com.fcul.adro.indoorNavigationSystem.model.dto.json;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class FrequencyDTO extends WINSDTO {

  private static final long serialVersionUID = -2139525529083182763L;

  private int rssi;
  private String ssid;
  private String bssid;

}

package com.fcul.adro.indoorNavigationSystem.mappers;

import com.fcul.adro.indoorNavigationSystem.model.dto.json.FrequencyDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.Frequency;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface FrequencyMapper {

  FrequencyMapper INSTANCE = Mappers.getMapper(FrequencyMapper.class);

  @Mappings({
      @Mapping(source = "accessPoint.bssid", target = "bssid"),
      @Mapping(source = "accessPoint.ssid", target = "ssid")
  })
  FrequencyDTO frequencyToDTO(Frequency frequency);

  Frequency dtoToFrequency(FrequencyDTO entity);

  AccessPoint dtoToAccessPoint(FrequencyDTO entity);

}

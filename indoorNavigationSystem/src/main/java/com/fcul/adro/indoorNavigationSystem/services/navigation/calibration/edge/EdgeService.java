package com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.edge;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.Edge;
import java.util.List;
import java.util.UUID;

public interface EdgeService {

  List<Edge> fetchEdges();

  UUID createEdgeFromControlPoints(ControlPoint parentControlPoint, ControlPoint childControlPoint);

}

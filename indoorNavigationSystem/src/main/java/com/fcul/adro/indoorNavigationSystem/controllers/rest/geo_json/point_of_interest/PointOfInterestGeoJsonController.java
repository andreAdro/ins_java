package com.fcul.adro.indoorNavigationSystem.controllers.rest.geo_json.point_of_interest;

import com.fcul.adro.indoorNavigationSystem.mappers.PointOfInterestMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestService;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "geo-json/points-of-interest")
@Api(tags = "GeoJSON")
public class PointOfInterestGeoJsonController {

  private static final Logger LOGGER = LoggerFactory
      .getLogger(PointOfInterestGeoJsonController.class);

  private PointOfInterestMapper pointOfInterestMapper;
  private PointOfInterestService pointOfInterestService;

  @Autowired
  public PointOfInterestGeoJsonController(PointOfInterestMapper pointOfInterestMapper,
      @PointOfInterestServiceBean PointOfInterestService pointOfInterestService) {
    this.pointOfInterestMapper = pointOfInterestMapper;
    this.pointOfInterestService = pointOfInterestService;
  }

  @GetMapping
  @ApiOperation("Retrieves all known point of interest as a GeoJson representation.")
  public ResponseEntity getGeoJsonPointOfInterest() {
    List<PointOfInterest> pointOfInterests = this.pointOfInterestService.fetchPointsOfInterest();
    List<FeatureObjectDTO> geoJsonDoors = pointOfInterests
        .stream()
        .map(this.pointOfInterestMapper::pointOfInterestToGeoJson)
        .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonDoors);
  }

  @PostMapping
  @ApiOperation("Creates a point of interest based on the provided GeoJson representation.")
  public ResponseEntity createGeoJsonPointOfInterest(
      @RequestBody FeatureObjectDTO geoJsonPointOfInterest) {
    PointOfInterest pointOfInterest = this.pointOfInterestMapper
        .geoJsonToPointOfInterest(geoJsonPointOfInterest);
    UUID doorId = this.pointOfInterestService.createPointOfInterest(pointOfInterest);
    return ResponseEntity.status(HttpStatus.CREATED).body(doorId);
  }

  @GetMapping(path = "/{id}")
  @ApiOperation("Retrieves a point of interest based on the provided identifier as a GeoJson representation.")
  public ResponseEntity getGeoJsonPointOfInterest(@PathVariable("id") UUID id) {
    PointOfInterest fetchedPointOfInterest = this.pointOfInterestService
        .fetchPointOfInterestById(id);
    FeatureObjectDTO geoJsonDoor = this.pointOfInterestMapper
        .pointOfInterestToGeoJson(fetchedPointOfInterest);
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonDoor);
  }

  @PutMapping(path = "{id}")
  @ApiOperation("Updates a point of interest with the provided identifier, based on the provided GeoJson representation.")
  public ResponseEntity updateGeoJsonPointOfInterest(@PathVariable(name = "id") UUID id,
      @RequestBody FeatureObjectDTO featureObjectDTODoor) {
    PointOfInterest pointOfInterest = this.pointOfInterestMapper
        .geoJsonToPointOfInterest(featureObjectDTODoor);
    this.pointOfInterestService.updatePointOfInterest(id, pointOfInterest);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

  @DeleteMapping(path = "{id}")
  @ApiOperation("Deletes a point of interest based on the provided identifier.")
  public ResponseEntity deleteGeoJsonPointOfInterest(@PathVariable(name = "id") UUID id) {
    this.pointOfInterestService.deletePointOfInterest(id);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

}

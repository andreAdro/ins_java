package com.fcul.adro.indoorNavigationSystem.model.dto.geo_json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeaturesCollectionDTO {

  @NotNull
  @ApiModelProperty(allowableValues = "Feature, FeatureCollection")
  @SerializedName("type")
  private FeatureType type;

  @SerializedName("features")
  private List<FeatureObjectDTO> features;

}

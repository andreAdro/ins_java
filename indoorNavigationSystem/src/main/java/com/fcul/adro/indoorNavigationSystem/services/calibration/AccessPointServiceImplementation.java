package com.fcul.adro.indoorNavigationSystem.services.calibration;

import com.fcul.adro.indoorNavigationSystem.error.exception.ResourceNotFoundException;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import com.fcul.adro.indoorNavigationSystem.repository.AccessPointRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AccessPointServiceBean
public class AccessPointServiceImplementation implements AccessPointService {

  private AccessPointRepository accessPointRepository;

  @Autowired
  public AccessPointServiceImplementation(AccessPointRepository accessPointRepository) {
    this.accessPointRepository = accessPointRepository;
  }

  @Override
  public List<AccessPoint> fetchAccessPointByBSSID(List<String> bssids) {
    return this.accessPointRepository.findAccessPointsByBSSIDs(bssids);
  }

  @Override
  public AccessPoint fetchAccessPointByBSSID(String bssid) {
    return accessPointRepository
        .getAccessPointByBSSID(bssid)
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    String.format("Access Point with id %s not found.", bssid)));
  }

  @Override
  public UUID createAccessPoint(AccessPoint accessPoint) {
    return accessPointRepository.save(accessPoint).getIdentifier();
  }
}

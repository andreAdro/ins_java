package com.fcul.adro.indoorNavigationSystem.repository;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuildingRepository extends JpaRepository<Building, UUID> {

//  @Query("select r from Room r where r.name = :roomName")
//  Room findRoomByName(@Param("roomName") String roomName);

}

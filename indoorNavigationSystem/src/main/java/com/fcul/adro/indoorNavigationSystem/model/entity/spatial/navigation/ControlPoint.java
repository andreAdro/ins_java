package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation;

import static java.util.Comparator.comparing;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.Frequency;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.StoredProcedureParameter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@Entity(name = "CONTROL_POINTS")
@PrimaryKeyJoinColumn(name = "POINT_ID")
@NamedStoredProcedureQueries({
    @NamedStoredProcedureQuery(
        name = "controlPointsGeoDistance",
        procedureName = "controlPointsGeoDistance",
        resultClasses = ControlPoint.class,
        parameters = {
            @StoredProcedureParameter(
                mode = ParameterMode.IN,
                name = "sourceLatitude",
                type = double.class),
            @StoredProcedureParameter(
                mode = ParameterMode.IN,
                name = "sourceLongitude",
                type = double.class),
            @StoredProcedureParameter(mode = ParameterMode.IN, name = "bufferSize", type = double.class)
        })
})
public class ControlPoint extends Point {

  private static final long serialVersionUID = -3846407556077096061L;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "controlPoint")
  private Set<Frequency> frequencies;

  @Include
  @Column(name = "NAME", nullable = false, unique = true)
  private String name = "";

  @Column(name = "TIME_DISTORTION")
  private Double timeDistortion = 0.0;

  public void addFrequency(Frequency frequency) {
    frequency.setControlPoint(this);
    frequencies.add(frequency);
    calculateDistortion();
  }

  private void calculateDistortion() {
    if (CollectionUtils.isNotEmpty(frequencies)) {
      Map<AccessPoint, List<Frequency>> groupedFrequenciesByAccessPoint =
          this.frequencies.stream().collect(Collectors.groupingBy(Frequency::getAccessPoint));
      List<Double> averagedFrequencies = new ArrayList<>();
      for (Entry<AccessPoint, List<Frequency>> accessPointListEntry :
          groupedFrequenciesByAccessPoint.entrySet()) {

        Double average = Math.abs(accessPointListEntry.getValue().stream()
            .mapToDouble(Frequency::getRssi).average().getAsDouble());

        Double highest = Math.abs(Double.valueOf(accessPointListEntry.getValue().stream()
            .max(comparing(frequency -> Math.abs(frequency.getRssi()))).get().getRssi()));

        Double lowest = Math.abs(Double.valueOf(accessPointListEntry.getValue().stream()
            .min(comparing(frequency -> Math.abs(frequency.getRssi()))).get().getRssi()));

        if (highest - average > average - lowest) {
          Double ruleOfThree = Math.abs(100 - ruleOfThree(average, highest));
          averagedFrequencies.add(ruleOfThree);
        } else {
          Double ruleOfThree = Math.abs(100 - ruleOfThree(average, lowest));
          averagedFrequencies.add(ruleOfThree);
        }
      }
      timeDistortion = averagedFrequencies
          .stream()
          .mapToDouble(value -> value)
          .average()
          .getAsDouble();
    }
  }

  private Double ruleOfThree(Double average, Double higherOffSetValue) {
    return (higherOffSetValue * 100) / average;
  }
}

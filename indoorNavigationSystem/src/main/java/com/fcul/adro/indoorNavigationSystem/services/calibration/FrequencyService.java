package com.fcul.adro.indoorNavigationSystem.services.calibration;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.Frequency;
import java.util.List;
import java.util.UUID;

public interface FrequencyService {

  List<Frequency> fetchFrequencies(ControlPoint controlPoint);

  UUID createFrequency(ControlPoint controlPoint, AccessPoint accessPoint, Frequency frequency);

}

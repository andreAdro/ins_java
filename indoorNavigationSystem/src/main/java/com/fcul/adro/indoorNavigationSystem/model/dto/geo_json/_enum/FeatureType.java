package com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel
public enum FeatureType {

  @JsonProperty("Feature")
  FEATURE,
  @JsonProperty("FeatureCollection")
  FEATURE_COLLECTION
}

package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polygon;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "ROOMS")
@PrimaryKeyJoinColumn(name = "IDENTIFIER")
public class Room extends Polygon {

  private static final long serialVersionUID = -8232018833197963092L;

  @Column(name = "name")
  private String name;

  @ManyToOne(fetch = FetchType.EAGER, optional = false)
  @JoinColumn(name = "FLOOR_ID")
  private Floor floor;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinTable(name = "ROOMS_POINTS_OF_INTEREST",
      joinColumns = @JoinColumn(name = "ROOM_ID", referencedColumnName = "IDENTIFIER"),
      inverseJoinColumns = @JoinColumn(name = "POINT_OF_INTEREST_ID")
  )
  private Set<PointOfInterest> pointsOfInterest;

  public void update(Room room) {
    super.update(room);
    this.name = room.getName();
    this.floor = room.getFloor();
    this.pointsOfInterest = room.getPointsOfInterest();
  }
}

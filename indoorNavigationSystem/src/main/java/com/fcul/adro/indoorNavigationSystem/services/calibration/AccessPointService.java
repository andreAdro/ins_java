package com.fcul.adro.indoorNavigationSystem.services.calibration;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import java.util.List;
import java.util.UUID;

public interface AccessPointService {

  List<AccessPoint> fetchAccessPointByBSSID(List<String> bssids);

  AccessPoint fetchAccessPointByBSSID(String bssid);

  UUID createAccessPoint(AccessPoint accessPoint);
}

package com.fcul.adro.indoorNavigationSystem.repository;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccessPointRepository extends JpaRepository<AccessPoint, UUID> {

  @Query("select ap from ACCESS_POINTS ap where ap.bssid = :bssid")
  Optional<AccessPoint> getAccessPointByBSSID(@Param("bssid") String bssid);

  @Query("select ap from ACCESS_POINTS ap where ap.bssid in (:bssids)")
  List<AccessPoint> findAccessPointsByBSSIDs(@Param("bssids") List<String> bssids);
}

package com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GeometryType {
  @JsonProperty("Point")
  POINT,
  @JsonProperty("MultiPoint")
  MULTI_POINT,
  @JsonProperty("LineString")
  LINE_STRING,
  @JsonProperty("MultiLineString")
  MULTI_LINE_STRING,
  @JsonProperty("Polygon")
  POLYGON,
  @JsonProperty("MultiPolygon")
  MULTI_POLYGON,
  @JsonProperty("GeometryCollection")
  GEOMETRY_COLLECTION
}

package com.fcul.adro.indoorNavigationSystem.services.point_of_interest.room;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import java.util.List;

public interface RoomService {

  List<Room> fetchRooms(PointOfInterest pointOfInterest);

}

package com.fcul.adro.indoorNavigationSystem.controllers.rest.navigation.location;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fcul.adro.indoorNavigationSystem.mappers.ControlPointMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.json.FrequencyDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.Frequency;
import com.fcul.adro.indoorNavigationSystem.services.calibration.AccessPointService;
import com.fcul.adro.indoorNavigationSystem.services.calibration.AccessPointServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point.ControlPointService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point.ControlPointServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.edge.EdgeService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.edge.EdgeServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("navigation/control-points")
@Api(tags = "Navigation")
public class ControlPointController {

  private ControlPointService controlPointService;
  private ControlPointMapper controlPointMapper;
  private EdgeService edgeService;
  private AccessPointService accessPointService;

  @Autowired
  public ControlPointController(
      @ControlPointServiceBean ControlPointService controlPointService,
      ControlPointMapper controlPointMapper,
      @EdgeServiceBean EdgeService edgeService,
      @AccessPointServiceBean AccessPointService accessPointService) {
    this.controlPointService = controlPointService;
    this.controlPointMapper = controlPointMapper;
    this.edgeService = edgeService;
    this.accessPointService = accessPointService;
  }

  @GetMapping
  @ApiOperation("Retrieves all known control points as a GeoJSON representation.")
  public ResponseEntity getControlPoints(
      @RequestParam(value = "frequencies", required = false) String frequenciesString)
      throws IOException {
    if (StringUtils.isEmpty(frequenciesString)) {
      List<FeatureObjectDTO> geoJsonControlPoints = getAllControlPoints();
      return ResponseEntity.status(HttpStatus.OK).body(geoJsonControlPoints);
    }
    List<FeatureObjectDTO> geoJsonControlPoints =
        getControlPointsSortedByDistanceFromFrequencies(frequenciesString);
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonControlPoints);
  }

  private List<FeatureObjectDTO> getControlPointsSortedByDistanceFromFrequencies(
      String frequenciesString) throws IOException {
    List<FrequencyDTO> frequencies = jsonParameterToFrequencies(frequenciesString);
    List<ControlPoint> nearestControlPoints = getNearestControlPointsByFrequencies(frequencies);
    return nearestControlPoints.stream()
        .map(this.controlPointMapper::controlPointToGeoJson)
        .collect(Collectors.toList());
  }

  private List<FrequencyDTO> jsonParameterToFrequencies(String frequenciesString)
      throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(frequenciesString, new TypeReference<List<FrequencyDTO>>() {
    });
  }

  private List<ControlPoint> getNearestControlPointsByFrequencies(List<FrequencyDTO> frequencies) {
    List<AccessPoint> filteredAccessPoints = fetchAccessPointsByBSSID(frequencies);
    List<ControlPoint> possibleControlPoints =
        obtainPossibleControlPointsByRSSIMeasurement(frequencies, filteredAccessPoints);
    return getSortedControlPointsByOccurrence(possibleControlPoints);
  }

  /**
   * Counts the number of control points objects and returns the most "voted" one
   *
   * @param possibleControlPoints list of possible control points. can contain duplicates
   * @return a list of controlPoints where from the first element to the last one corresponds to the
   * number ov occurrences provided in @param possibleControlPoints
   */
  private List<ControlPoint> getSortedControlPointsByOccurrence(
      List<ControlPoint> possibleControlPoints) {
    Map<ControlPoint, Integer> controlPointsOccurrences = new LinkedHashMap<>();
    for (ControlPoint possibleControlPoint : possibleControlPoints) {
      controlPointsOccurrences.put(
          possibleControlPoint, Collections.frequency(possibleControlPoints, possibleControlPoint));
    }
    return controlPointsOccurrences.entrySet().stream()
        .sorted(Entry.<ControlPoint, Integer>comparingByValue().reversed())
        .map(Entry::getKey)
        .collect(Collectors.toList());
  }

  private List<ControlPoint> obtainPossibleControlPointsByRSSIMeasurement(
      List<FrequencyDTO> frequencies, List<AccessPoint> filteredAccessPoints) {
    List<ControlPoint> possibleControlPoints = new ArrayList<>();
    for (AccessPoint filteredAccessPoint : filteredAccessPoints) {
      for (FrequencyDTO frequencyDTO : frequencies) {
        if (frequencyDTO.getBssid().equals(filteredAccessPoint.getBssid())) {
          filteredAccessPoint.getFrequencies().stream()
              .peek(frequency -> frequency.measure(frequencyDTO.getRssi())) // measures
              .min(Comparator.comparing(
                  Frequency::getMeasurement)) // sorts and obtains smallest controlPoint distance |
              // closest one
              .ifPresent(frequency -> possibleControlPoints
                  .add(frequency.getControlPoint())); // adds to list of possible controlPoints. if
          // present only used due to lambda
        }
      }
    }
    return possibleControlPoints;
  }

  private List<AccessPoint> fetchAccessPointsByBSSID(List<FrequencyDTO> frequencies) {
    return this.accessPointService.fetchAccessPointByBSSID(
        frequencies.stream().map(FrequencyDTO::getBssid).collect(Collectors.toList()));
  }

  private List<FeatureObjectDTO> getAllControlPoints() {
    List<ControlPoint> controlPoints = this.controlPointService.fetchControlPoints();
    return controlPoints.stream()
        .map(this.controlPointMapper::controlPointToGeoJson)
        .collect(Collectors.toList());
  }

  @PostMapping
  @ApiOperation("Creates a control point based on the provided GeoJson representation.")
  public ResponseEntity createControlPoint(@RequestBody FeatureObjectDTO geoJsonControlPoint) {
    ControlPoint controlPoint = this.controlPointMapper.geoJsonToControlPoint(geoJsonControlPoint);
    UUID controlPointId = this.controlPointService.createControlPoint(controlPoint);
    return ResponseEntity.status(HttpStatus.CREATED).body(controlPointId);
  }

  @GetMapping(path = "/{controlPointId}")
  @ApiOperation("Retrieves a control points as a GeoJSON representation based on the provided id.")
  public ResponseEntity getControlPointById(@PathVariable("controlPointId") UUID id) {
    ControlPoint fetchedControlPoint = this.controlPointService.fetchControlPointById(id);
    FeatureObjectDTO geoJsonControlPoint =
        this.controlPointMapper.controlPointToGeoJson(fetchedControlPoint);
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonControlPoint);
  }

  @PostMapping(path = "/{controlPointId}")
  @ApiOperation(
      "Creates a control point nested on the provided control point id and based on the provided GeoJson representation.")
  public ResponseEntity createNestedControlPoint(
      @PathVariable("controlPointId") UUID id, @RequestBody FeatureObjectDTO geoJsonControlPoint) {
    // Validations
    ControlPoint parsedControlPoint =
        this.controlPointMapper.geoJsonToControlPoint(geoJsonControlPoint);
    ControlPoint parentControlPoint = this.controlPointService.fetchControlPointById(id);
    UUID controlPointId = this.controlPointService.createControlPoint(parsedControlPoint);
    // Creates child
    ControlPoint childControlPoint = this.controlPointService.fetchControlPointById(id);
    // Links parent with child
    this.edgeService.createEdgeFromControlPoints(parentControlPoint, childControlPoint);
    return ResponseEntity.status(HttpStatus.CREATED).body(controlPointId);
  }
}

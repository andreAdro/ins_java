package com.fcul.adro.indoorNavigationSystem.services.floor;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import java.util.List;
import java.util.UUID;

public interface FloorService {

  List<Floor> fetchFloors(Building building);

  UUID createFloor(Building building, Floor floor);

  Floor fetchFloorById(Building building, UUID id);

  void updateFloor(Building building, UUID id, Floor floor);

  void deleteFloor(Building building, UUID id);

}

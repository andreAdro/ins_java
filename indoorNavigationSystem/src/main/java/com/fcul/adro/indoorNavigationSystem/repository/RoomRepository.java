package com.fcul.adro.indoorNavigationSystem.repository;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, UUID> {

  @Query("select r from ROOMS r where r.floor.id = :floorId")
  List<Room> findRoomsByFloorId(@Param("floorId") UUID floorId);

  @Query("select r from ROOMS r where r.floor.id = :floorId and r.identifier = :id")
  Optional<Room> findRoomByFloorIdAndIdentifier(@Param("floorId") UUID floorId,
      @Param("id") UUID id);

  @Query("select r from ROOMS r left outer join r.pointsOfInterest p where p.identifier = :pointOfInterestId")
  List<Room> findRoomsByPointOfInterestId(@Param("pointOfInterestId") UUID identifier);
}

package com.fcul.adro.indoorNavigationSystem.repository;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.Frequency;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FrequencyRepository extends JpaRepository<Frequency, UUID> {

  @Query("select f from FREQUENCIES f left outer join f.controlPoint cp where cp.identifier = :controlPointId")
  List<Frequency> fetchFrequenciesByControlPointId(@Param("controlPointId") UUID controlPointId);

}

package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry;

import com.fcul.adro.indoorNavigationSystem.model.entity.WINSEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "POINTS")
@Inheritance(strategy = InheritanceType.JOINED)
public class Point extends WINSEntity<Point> {

  private static final long serialVersionUID = 642488291861241230L;

  @Include
  @Column(name = "LATITUDE")
  private Double latitude;

  @Include
  @Column(name = "LONGITUDE")
  private Double longitude;

  @Include
  @Column(name = "ALTITUDE")
  private Double altitude;

  @Override
  public void update(Point point) {
    this.longitude = point.getLongitude();
    this.latitude = point.getLatitude();
    this.altitude = point.getAltitude();
  }
}

package com.fcul.adro.indoorNavigationSystem.services.navigation.path.algorithms.dijkstra;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.Edge;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.edge.EdgeService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.edge.EdgeServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.navigation.path.algorithms.PathFinderService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@DjikstraServiceBean
public class DijkstraServiceImpl implements PathFinderService {

  private List<Edge> edges = new ArrayList<>();
  private Set<ControlPoint> settledNodes;
  private Set<ControlPoint> unSettledNodes;
  private Map<ControlPoint, ControlPoint> predecessors;
  private Map<ControlPoint, Double> distance;
  private EdgeService edgeService;

  @Autowired
  public DijkstraServiceImpl(@EdgeServiceBean EdgeService edgeService) {
    this.edgeService = edgeService;
  }

  @Override
  public List<Point> getPathAsPoints(ControlPoint start, ControlPoint goal) {
    build(start);
    LinkedList<Point> path = new LinkedList<>();
    ControlPoint step = goal;
    // check if a path exists
    if (predecessors.get(step) == null) {
      return null;
    }
    path.add(step);
    while (predecessors.get(step) != null) {
      step = predecessors.get(step);
      path.add(step);
    }
    // Put it into the correct order
    Collections.reverse(path);
    return path;
  }

  private void build(ControlPoint start) {
    this.edges = this.edgeService.fetchEdges(); // TODO needs optimization
    this.settledNodes = new HashSet<>();
    this.unSettledNodes = new HashSet<>();
    this.distance = new HashMap<>();
    this.predecessors = new HashMap<>();
    this.distance.put(start, 0.0);
    this.unSettledNodes.add(start);
    while (unSettledNodes.size() > 0) {
      ControlPoint node = getMinimum(unSettledNodes);
      settledNodes.add(node);
      unSettledNodes.remove(node);
      findMinimalDistances(node);
    }
  }

  private void findMinimalDistances(ControlPoint node) {
    List<ControlPoint> adjacentNodes = getNeighbors(node);
    for (ControlPoint target : adjacentNodes) {
      if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
        distance.put(target, getShortestDistance(node) + getDistance(node, target));
        predecessors.put(target, node);
        unSettledNodes.add(target);
      }
    }
  }

  private double getDistance(ControlPoint node, ControlPoint target) {
    for (Edge edge : this.edges) {
      if (edge.getStart().equals(node) && edge.getEnd().equals(target)) {
        return edge.getDistance();
      }
    }
    throw new RuntimeException("Should not happen");
  }

  private List<ControlPoint> getNeighbors(ControlPoint node) {
    List<ControlPoint> neighbors = new ArrayList<>();
    for (Edge edge : this.edges) {
      if (edge.getStart().equals(node) && !isSettled(edge.getEnd())) {
        neighbors.add(edge.getEnd());
      }
    }
    return neighbors;
  }

  private ControlPoint getMinimum(Set<ControlPoint> points) {
    ControlPoint minimum = null;
    for (ControlPoint vertex : points) {
      if (minimum == null) {
        minimum = vertex;
      } else {
        if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
          minimum = vertex;
        }
      }
    }
    return minimum;
  }

  private boolean isSettled(ControlPoint vertex) {
    return settledNodes.contains(vertex);
  }

  private Double getShortestDistance(ControlPoint destination) {
    Double d = distance.get(destination);
    if (d == null) {
      return Double.MAX_VALUE;
    } else {
      return d;
    }
  }
}

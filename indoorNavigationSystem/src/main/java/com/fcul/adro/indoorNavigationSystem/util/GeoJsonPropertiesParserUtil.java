package com.fcul.adro.indoorNavigationSystem.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import java.io.IOException;
import java.util.List;

public final class GeoJsonPropertiesParserUtil {

  private GeoJsonPropertiesParserUtil() {
  }

  public static List<FeatureObjectDTO> objectToFeatureObjectDTOS(Object object) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      String jsonArray = mapper.writeValueAsString(object);
      CollectionType javaType = mapper.getTypeFactory()
          .constructCollectionType(List.class, FeatureObjectDTO.class);
      return mapper.readValue(jsonArray, javaType);
    } catch (IOException e) {
      throw new RuntimeException("Unable to parse JSON @ properties for field pointsOfInterest");
    }
  }
}

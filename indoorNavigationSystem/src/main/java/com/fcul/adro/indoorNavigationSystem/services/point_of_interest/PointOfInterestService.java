package com.fcul.adro.indoorNavigationSystem.services.point_of_interest;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import java.util.List;
import java.util.UUID;

public interface PointOfInterestService {

  List<PointOfInterest> fetchPointsOfInterest();

  UUID createPointOfInterest(PointOfInterest pointOfInterest);

  PointOfInterest fetchPointOfInterestById(UUID id);

  void updatePointOfInterest(UUID id, PointOfInterest pointOfInterest);

  void deletePointOfInterest(UUID id);
}

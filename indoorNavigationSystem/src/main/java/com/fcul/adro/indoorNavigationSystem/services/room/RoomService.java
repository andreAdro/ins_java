package com.fcul.adro.indoorNavigationSystem.services.room;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import java.util.List;
import java.util.UUID;

public interface RoomService {

  List<Room> fetchRooms(Floor floor);

  UUID createRoom(Floor floor, Room room);

  Room fetchRoomById(Floor floor, UUID id);

  void updateRoom(Floor floor, UUID id, Room room);

  void deleteRoom(Floor floor, UUID id);

}

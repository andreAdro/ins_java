package com.fcul.adro.indoorNavigationSystem.mappers.geo_json;

import static com.fcul.adro.indoorNavigationSystem.util.GeoJsonPropertiesParserUtil.objectToFeatureObjectDTOS;

import com.fcul.adro.indoorNavigationSystem.mappers.PointOfInterestMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Mapper
@Component
public abstract class PropertiesMapper {

  @Autowired
  protected PointOfInterestMapper pointOfInterestMapper;

  public Set<PointOfInterest> propertiesToPointsOfInterest(Object pointsOfInterestArray) {
    if (pointsOfInterestArray instanceof List) {
      List<FeatureObjectDTO> featureObjectDTOS = objectToFeatureObjectDTOS(pointsOfInterestArray);
      return featureObjectDTOS
          .stream()
          .map(featureObjectDTO
              -> this.pointOfInterestMapper.geoJsonToPointOfInterest(featureObjectDTO))
          .collect(Collectors.toSet());
    }
    return new LinkedHashSet<>();
  }

  public void extractPointsOfInterestToProperties(Set<PointOfInterest> pointsOfInterest,
      Map<String, Object> properties) {
    if (CollectionUtils.isNotEmpty(pointsOfInterest)) {
      List<FeatureObjectDTO> geoJsonPointsOfInterest = pointsOfInterest
          .stream()
          .map(pointOfInterest ->
              this.pointOfInterestMapper.pointOfInterestToGeoJson(pointOfInterest)
          ).collect(Collectors.toList());
      properties.put("pointsOfInterest", geoJsonPointsOfInterest);
    }
  }

}

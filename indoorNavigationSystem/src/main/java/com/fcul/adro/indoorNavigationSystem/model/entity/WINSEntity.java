package com.fcul.adro.indoorNavigationSystem.model.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.UpdateTimestamp;

@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@MappedSuperclass
public abstract class WINSEntity<T> implements Serializable {

  private static final long serialVersionUID = 5232221529717121993L;

  @Include
  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(
      name = "UUID",
      strategy = "org.hibernate.id.UUIDGenerator",
      parameters = {
          @Parameter(
              name = "uuid_gen_strategy_class",
              value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
          )
      }
  )
  @Column(name = "IDENTIFIER", updatable = false, nullable = false, columnDefinition = "BINARY(16)")
//  @GeneratedValue(generator = "uuid2")
//  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  private UUID identifier;

  @CreationTimestamp
  @Column(name = "CREATED_DATE", columnDefinition = "DATETIME(6)")
  private Instant createdDate;

  @UpdateTimestamp
  @Column(name = "UPDATED_DATE", columnDefinition = "DATETIME(6)")
  private Instant updatedDate;

  public abstract void update(T t);
}

package com.fcul.adro.indoorNavigationSystem.services.point_of_interest;

import com.fcul.adro.indoorNavigationSystem.error.exception.ResourceNotFoundException;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.repository.PointOfInterestRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@PointOfInterestServiceBean
public class PointOfInterestServiceImpl implements PointOfInterestService {

  private PointOfInterestRepository pointOfInterestRepository;

  @Autowired
  public PointOfInterestServiceImpl(PointOfInterestRepository pointOfInterestRepository) {
    this.pointOfInterestRepository = pointOfInterestRepository;
  }

  @Override
  public List<PointOfInterest> fetchPointsOfInterest() {
    return this.pointOfInterestRepository.findAll();
  }

  @Override
  public UUID createPointOfInterest(PointOfInterest pointOfInterest) {
    PointOfInterest persistedPointOfInterest = this.pointOfInterestRepository.save(pointOfInterest);
    return persistedPointOfInterest.getIdentifier();
  }

  @Override
  public PointOfInterest fetchPointOfInterestById(UUID id) {
    Optional<PointOfInterest> maybeDoor = this.pointOfInterestRepository.findById(id);
    return maybeDoor.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public void updatePointOfInterest(UUID id, PointOfInterest pointOfInterest) {
    PointOfInterest fetchedPointOfInterest = this.fetchPointOfInterestById(id);
    fetchedPointOfInterest.update(pointOfInterest);
    this.pointOfInterestRepository.save(fetchedPointOfInterest);
  }

  @Override
  public void deletePointOfInterest(UUID id) {
    this.pointOfInterestRepository.deleteById(id);
  }
}


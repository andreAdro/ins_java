package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical;

import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FloorType;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polygon;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "FLOORS")
@PrimaryKeyJoinColumn(name = "IDENTIFIER")
public class Floor extends Polygon {

  private static final long serialVersionUID = 158749745384860337L;

  @Enumerated(value = EnumType.STRING)
  @Column(name = "TYPE")
  private FloorType type;

  @ManyToOne(fetch = FetchType.EAGER, optional = false)
  @JoinColumn(name = "BUILDING_ID")
  private Building building;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "floor", orphanRemoval = true)
  private Set<Room> rooms;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinTable(name = "FLOORS_POINTS_OF_INTEREST",
      joinColumns = @JoinColumn(name = "FLOOR_ID", referencedColumnName = "IDENTIFIER"),
      inverseJoinColumns = @JoinColumn(name = "POINT_OF_INTEREST_ID")
  )
  private Set<PointOfInterest> pointsOfInterest;

  public void update(Floor floor) {
    super.update(floor);
    this.type = floor.getType();
    this.building = floor.getBuilding();
    this.rooms = floor.getRooms();
    this.pointsOfInterest = building.getPointsOfInterest();
  }
}

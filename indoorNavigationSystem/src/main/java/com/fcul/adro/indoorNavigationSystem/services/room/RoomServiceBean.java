package com.fcul.adro.indoorNavigationSystem.services.room;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier(value = "RoomServiceBean")
@Retention(RUNTIME)
@Target({TYPE, PARAMETER})
public @interface RoomServiceBean {

}

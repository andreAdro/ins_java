package com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point;

import com.fcul.adro.indoorNavigationSystem.error.exception.ResourceNotFoundException;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.repository.control_point.ControlPointRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@ControlPointServiceBean
public class ControlPointServiceImpl implements ControlPointService {

  private ControlPointRepository controlPointRepository;

  @Autowired
  public ControlPointServiceImpl(ControlPointRepository controlPointRepository) {
    this.controlPointRepository = controlPointRepository;
  }

  @Override
  public List<ControlPoint> fetchControlPoints() {
    return this.controlPointRepository.findAll();
  }

  @Override
  public UUID createControlPoint(ControlPoint controlPoint) {
    ControlPoint persistedControlPoint = this.controlPointRepository.save(controlPoint);
    return persistedControlPoint.getIdentifier();
  }

  @Override
  public ControlPoint fetchControlPointById(UUID id) {
    Optional<ControlPoint> maybeControlPoint = this.controlPointRepository.findById(id);
    return maybeControlPoint.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public Optional<ControlPoint> fetchControlPointByCoordinates(
      Double latitude, Double longitude, Double radius) {
    List<ControlPoint> controlPointsByDistance =
        this.controlPointRepository.getControlPointsByDistance(latitude, longitude, radius);
    if (CollectionUtils.isNotEmpty(controlPointsByDistance)) {
      return Optional.of(controlPointsByDistance.get(0));
    }
    return Optional.empty();
  }

  @Override
  public Optional<ControlPoint> fetchControlPointByCoordinates(Double latitude, Double longitude) {
    ControlPoint controlPoint =
        this.controlPointRepository.findControlPointByCoordinates(latitude, longitude);
    if (null == controlPoint) {
      return Optional.empty();
    }
    return Optional.of(controlPoint);
  }
}

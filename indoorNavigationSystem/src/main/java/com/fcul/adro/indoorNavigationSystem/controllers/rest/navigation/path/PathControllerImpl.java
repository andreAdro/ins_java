package com.fcul.adro.indoorNavigationSystem.controllers.rest.navigation.path;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.EdgeMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point.ControlPointService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point.ControlPointServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.navigation.path.PathService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.path.PathServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestService;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("navigation/paths")
@Api(tags = "Navigation")
public class PathControllerImpl {

  private PathService pathService;
  private ControlPointService controlPointService;
  private PointOfInterestService pointOfInterestService;
  private EdgeMapper edgeMapper;

  @Autowired
  public PathControllerImpl(
      @PathServiceBean PathService pathService,
      @ControlPointServiceBean ControlPointService controlPointService,
      @PointOfInterestServiceBean PointOfInterestService pointOfInterestService,
      EdgeMapper edgeMapper) {
    this.pathService = pathService;
    this.controlPointService = controlPointService;
    this.pointOfInterestService = pointOfInterestService;
    this.edgeMapper = edgeMapper;
  }

  @GetMapping
  @ApiOperation("Obtains a GeoJson PolyLine Feature with the corresponding path to take.")
  public ResponseEntity obtainRoute(
      @RequestParam("controlPointId") UUID controlPointId,
      @RequestParam("pointOfInterestId") UUID pointOfInterestId,
      @RequestParam(value = "iterations", required = false, defaultValue = "10") int iterations) {
    ControlPoint fetchedControlPoint =
        this.controlPointService.fetchControlPointById(controlPointId);
    PointOfInterest fetchedPointOfInterest =
        this.pointOfInterestService.fetchPointOfInterestById(pointOfInterestId);
    Optional<ControlPoint> controlPoint =
        obtainGeoRelatedControlPoint(fetchedPointOfInterest, iterations);
    if (controlPoint.isPresent()) {
      List<Point> pathInPoints =
          this.pathService.obtainPath(fetchedControlPoint, controlPoint.get());
      if (CollectionUtils.isNotEmpty(pathInPoints)) {
        FeatureObjectDTO geoJsonPolyline = this.edgeMapper.pointsToGeoJsonPolyline(pathInPoints);
        return ResponseEntity.status(HttpStatus.OK).body(geoJsonPolyline);
      }
    }
    return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
  }

  private Optional<ControlPoint> obtainGeoRelatedControlPoint(
      PointOfInterest fetchedPointOfInterest, int iterations) {
    double searchRadius = 1d;
    for (int i = 0; i < iterations; i++) {
      Optional<ControlPoint> controlPoint =
          this.controlPointService.fetchControlPointByCoordinates(
              fetchedPointOfInterest.getLatitude(),
              fetchedPointOfInterest.getLongitude(),
              searchRadius);
      if (controlPoint.isPresent()) {
        return controlPoint;
      }
      searchRadius = searchRadius * 1.3;
    }
    return Optional.empty();
  }
}

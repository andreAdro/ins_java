package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration;


import com.fcul.adro.indoorNavigationSystem.model.entity.SpatialRepresentation;
import com.fcul.adro.indoorNavigationSystem.model.entity.WINSEntity;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "ACCESS_POINTS")
public class AccessPoint extends WINSEntity<AccessPoint> implements SpatialRepresentation {

  private static final long serialVersionUID = -3085059537860026040L;

  @Column(name = "BSSID", unique = true)
  private String bssid;

  @Column(name = "SSID")
  private String ssid;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "accessPoint")
  private Set<Frequency> frequencies;

  @Override
  public void update(AccessPoint accessPoint) {
    //TODO
  }
}

package com.fcul.adro.indoorNavigationSystem.services.navigation.path;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.util.List;

public interface PathService {

  List<Point> obtainPath(ControlPoint startPoint, ControlPoint endPoint);

}

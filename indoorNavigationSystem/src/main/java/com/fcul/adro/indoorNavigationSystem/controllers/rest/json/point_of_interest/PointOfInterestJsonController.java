package com.fcul.adro.indoorNavigationSystem.controllers.rest.json.point_of_interest;

import com.fcul.adro.indoorNavigationSystem.mappers.PointOfInterestMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.json.PointOfInterestDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestService;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/points-of-interest")
@Api(tags = "JSON")
public class PointOfInterestJsonController {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      PointOfInterestJsonController.class);

  private PointOfInterestMapper pointOfInterestMapper;
  private PointOfInterestService pointOfInterestService;

  @Autowired
  public PointOfInterestJsonController(PointOfInterestMapper pointOfInterestMapper,
      @PointOfInterestServiceBean PointOfInterestService pointOfInterestService) {
    this.pointOfInterestMapper = pointOfInterestMapper;
    this.pointOfInterestService = pointOfInterestService;
  }

  @GetMapping
  @ApiOperation("Retrieves all known points of interest as a json representation.")
  public ResponseEntity getGeoJsonPointsOfInterest() {
    List<PointOfInterest> pointOfInterests = this.pointOfInterestService.fetchPointsOfInterest();
    List<PointOfInterestDTO> pointOfInterestDTO = pointOfInterests
        .stream()
        .map(this.pointOfInterestMapper::entityToDTO)
        .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(pointOfInterestDTO);
  }


  @GetMapping(path = "/{pointOfInterestId}")
  @ApiOperation("Retrieves a points of interest based on the provided identifier as a json representation.")
  public ResponseEntity getGeoJsonPointOfInterest(@PathVariable("pointOfInterestId") UUID id) {
    PointOfInterest fetchedPointOfInterest = this.pointOfInterestService
        .fetchPointOfInterestById(id);
    PointOfInterestDTO pointOfInterestDTO = this.pointOfInterestMapper
        .entityToDTO(fetchedPointOfInterest);
    return ResponseEntity.status(HttpStatus.OK).body(pointOfInterestDTO);
  }

}

package com.fcul.adro.indoorNavigationSystem.mappers.geo_json;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry.PolygonMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.GeometryDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FloorType;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polygon;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.google.common.base.CaseFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Mapper
@Component
public abstract class FloorMapper extends PolygonMapper {

  @Autowired
  protected PropertiesMapper propertiesMapper;

  /***
   *
   * @param featureObjectDTO
   * @return
   */
  public Floor geoJsonToFloor(FeatureObjectDTO featureObjectDTO) {
    Floor floor = new Floor();
    appendCoordinates(floor, featureObjectDTO);
    appendProperties(floor, featureObjectDTO);
    return floor;
  }

  private void appendCoordinates(Floor floor, FeatureObjectDTO featureObjectDTO) {
    Polygon polygon = super.geoJsonToPolygon(
        featureObjectDTO.getGeometry().getPolygonCoordinates(),
        featureObjectDTO.getGeometry().getType()
    );
    floor.update(polygon);
  }

  private void appendProperties(Floor floor, FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = featureObjectDTO.getProperties();
    appendType(floor, properties);
    appendPointsOfInterest(floor, properties);
  }

  private void appendType(Floor floor, Map<String, Object> properties) {
    Object floorType = properties.get("type");
    if (floorType instanceof String) {
      floor.setType(FloorType.valueOf(((String) floorType).toUpperCase()));
    }
  }

  private void appendPointsOfInterest(Floor floor, Map<String, Object> properties) {
    Object pointsOfInterestArray = properties.get("pointsOfInterest");
    Set<PointOfInterest> pointOfInterests =
        this.propertiesMapper.propertiesToPointsOfInterest(pointsOfInterestArray);
    floor.setPointsOfInterest(pointOfInterests);
  }

  /**
   *
   */
  public FeatureObjectDTO floorToGeoJson(Floor floor) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinates(floor, featureObjectDTO);
    extractProperties(floor, featureObjectDTO);
    extractOtherData(floor, featureObjectDTO);
    return featureObjectDTO;
  }

  private void extractCoordinates(Floor floor, FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = super.polygonToGeoJsonCoordinates(floor);
    featureObjectDTO.getGeometry().setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry().setType(floor.getGeometryType());
  }


  private void extractProperties(Floor floor, FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = new HashMap<>();
    extractType(floor, properties);
    extractPointsOfInterest(floor, properties);
    featureObjectDTO.setProperties(properties);
  }

  private void extractType(Floor floor, Map<String, Object> properties) {
    if (null != floor.getType()) {
      String camelCaseType = CaseFormat.UPPER_UNDERSCORE
          .to(CaseFormat.UPPER_CAMEL, floor.getType().toString());
      properties.put("type", camelCaseType);
    }
  }

  private void extractPointsOfInterest(Floor floor, Map<String, Object> properties) {
    this.propertiesMapper
        .extractPointsOfInterestToProperties(floor.getPointsOfInterest(), properties);
  }

  private void extractOtherData(Floor floor, FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(floor.getIdentifier());
  }

}

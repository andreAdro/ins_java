package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry;

import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.GeometryType;
import com.fcul.adro.indoorNavigationSystem.model.entity.WINSEntity;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "POLYGONS")
@Inheritance(strategy = InheritanceType.JOINED)
public class Polygon extends WINSEntity<Polygon> {

  private static final long serialVersionUID = 9127720855567470795L;

  @Column(name = "GEOMETRY_TYPE")
  @Enumerated(value = EnumType.STRING)
  private GeometryType geometryType;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinTable(name = "POLYGONS_POLYLINES",
      joinColumns = @JoinColumn(name = "POLYGON_ID", referencedColumnName = "IDENTIFIER"),
      inverseJoinColumns = @JoinColumn(name = "POLYLINE_ID")
  )
  @OrderBy(value = "identifier")
  private List<Polyline> polylines = new LinkedList<>();

  @Override
  public void update(Polygon polygon) {
    this.geometryType = polygon.getGeometryType();
    this.polylines = polygon.getPolylines();
  }
}

package com.fcul.adro.indoorNavigationSystem.services.room;

import com.fcul.adro.indoorNavigationSystem.error.exception.ResourceNotFoundException;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import com.fcul.adro.indoorNavigationSystem.repository.RoomRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RoomServiceBean
public class RoomServiceImpl implements RoomService {

  private RoomRepository roomRepository;

  @Autowired
  public RoomServiceImpl(RoomRepository roomRepository) {
    this.roomRepository = roomRepository;
  }

  @Override
  public List<Room> fetchRooms(Floor floor) {
    return this.roomRepository.findRoomsByFloorId(floor.getIdentifier());
  }

  @Override
  public UUID createRoom(Floor floor, Room room) {
    room.setFloor(floor);
    Room persistedRoom = this.roomRepository.save(room);
    return persistedRoom.getIdentifier();
  }

  @Override
  public Room fetchRoomById(Floor floor, UUID id) {
    Optional<Room> maybeRoom = this.roomRepository
        .findRoomByFloorIdAndIdentifier(floor.getIdentifier(), id);
    return maybeRoom.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public void updateRoom(Floor floor, UUID id, Room room) {
    Room fetchedRoom = this.fetchRoomById(floor, id);
    fetchedRoom.update(room);
    this.roomRepository.save(fetchedRoom);
  }

  @Override
  public void deleteRoom(Floor floor, UUID id) {
    Room fetchedRoom = this.fetchRoomById(floor, id);
    this.roomRepository.delete(fetchedRoom);
  }
}

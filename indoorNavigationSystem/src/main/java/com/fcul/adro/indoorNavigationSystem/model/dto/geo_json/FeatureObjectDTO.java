package com.fcul.adro.indoorNavigationSystem.model.dto.geo_json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel
public class FeatureObjectDTO {

  @ApiModelProperty(accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  private UUID id;

  @NotNull
  @ApiModelProperty(allowableValues = "Feature, FeatureCollection")
  @SerializedName("type")
  private FeatureType type;

  @Valid
  @SerializedName("geometry")
  private GeometryDTO geometry;

  private Map<String, Object> properties = new HashMap<>();

}

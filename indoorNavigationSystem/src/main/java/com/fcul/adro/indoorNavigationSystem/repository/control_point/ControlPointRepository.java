package com.fcul.adro.indoorNavigationSystem.repository.control_point;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ControlPointRepository extends JpaRepository<ControlPoint, UUID>,
    ControlPointCustomRepo {

  @Query("select  cp from CONTROL_POINTS cp where cp.latitude = :latitude and cp.longitude = :longitude")
  ControlPoint findControlPointByCoordinates(
      @Param("latitude") double latitude,
      @Param("longitude") double longitude
  );
}

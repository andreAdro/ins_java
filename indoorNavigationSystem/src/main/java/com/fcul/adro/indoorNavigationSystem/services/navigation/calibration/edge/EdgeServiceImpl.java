package com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.edge;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.Edge;
import com.fcul.adro.indoorNavigationSystem.repository.EdgeRepository;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@EdgeServiceBean
public class EdgeServiceImpl implements EdgeService {

  private EdgeRepository edgeRepository;
  private EntityManager entityManager;

  @Autowired
  public EdgeServiceImpl(EdgeRepository edgeRepository,
      EntityManager entityManager) {
    this.edgeRepository = edgeRepository;
    this.entityManager = entityManager;
  }

  @Override
  public List<Edge> fetchEdges() {
    return this.edgeRepository.findAll();
  }

  @Transactional(TxType.REQUIRES_NEW) // entity manager JTA context
  @Override
  public UUID createEdgeFromControlPoints(ControlPoint parentControlPoint,
      ControlPoint childControlPoint) {
    Edge edge = new Edge(parentControlPoint, childControlPoint);
    Edge persistedEdge = this.entityManager.merge(edge); // used merge to avoid detached entities
    return persistedEdge.getIdentifier();
  }
}

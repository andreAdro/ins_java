package com.fcul.adro.indoorNavigationSystem.services.calibration;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.Frequency;
import com.fcul.adro.indoorNavigationSystem.repository.FrequencyRepository;
import com.fcul.adro.indoorNavigationSystem.repository.control_point.ControlPointRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@FrequencyServiceBean
public class FrequencyServiceImplementation implements FrequencyService {

  private FrequencyRepository frequencyRepository;
  private ControlPointRepository controlPointRepository;

  @Autowired
  public FrequencyServiceImplementation(
      FrequencyRepository frequencyRepository, ControlPointRepository controlPointRepository) {
    this.frequencyRepository = frequencyRepository;
    this.controlPointRepository = controlPointRepository;
  }

  @Override
  public List<Frequency> fetchFrequencies(ControlPoint controlPoint) {
    return this.frequencyRepository.fetchFrequenciesByControlPointId(controlPoint.getIdentifier());
  }

  @Override
  public UUID createFrequency(
      ControlPoint controlPoint, AccessPoint accessPoint, Frequency frequency) {
    frequency.setAccessPoint(accessPoint);
    controlPoint.addFrequency(frequency);
    controlPointRepository.save(controlPoint);
    return frequency.getIdentifier();
  }
}

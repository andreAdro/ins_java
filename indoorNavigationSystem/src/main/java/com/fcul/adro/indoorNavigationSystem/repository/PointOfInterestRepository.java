package com.fcul.adro.indoorNavigationSystem.repository;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PointOfInterestRepository extends JpaRepository<PointOfInterest, UUID> {

}

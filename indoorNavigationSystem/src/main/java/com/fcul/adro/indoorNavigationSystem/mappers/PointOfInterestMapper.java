package com.fcul.adro.indoorNavigationSystem.mappers;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry.PointMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.GeometryDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.GeometryType;
import com.fcul.adro.indoorNavigationSystem.model.dto.json.PointOfInterestDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import java.util.HashMap;
import java.util.Map;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public abstract class PointOfInterestMapper extends PointMapper {

  public PointOfInterest geoJsonToPointOfInterest(FeatureObjectDTO featureObjectDTO) {
    PointOfInterest pointOfInterest = new PointOfInterest();
    appendCoordinate(pointOfInterest, featureObjectDTO);
    appendProperties(pointOfInterest, featureObjectDTO);
    return pointOfInterest;
  }

  public FeatureObjectDTO pointOfInterestToGeoJson(PointOfInterest pointOfInterest) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinate(pointOfInterest, featureObjectDTO);
    extractProperties(pointOfInterest, featureObjectDTO);
    extractOtherData(pointOfInterest, featureObjectDTO);
    return featureObjectDTO;
  }

  private void appendCoordinate(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    Point point = super.geoJsonCoordinateToPoint(
        featureObjectDTO.getGeometry().getPointCoordinates()
    );
    pointOfInterest.update(point);
  }

  private void extractCoordinate(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = super.pointToGeoJsonCoordinates(pointOfInterest);
    featureObjectDTO.getGeometry().setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry().setType(GeometryType.POINT);
  }

  private void appendProperties(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = featureObjectDTO.getProperties();
    Object doorName = properties.get("name");
    if (doorName instanceof String) {
      pointOfInterest.setName((String) doorName);
    }
  }

  private void extractProperties(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = new HashMap<>();
    properties.put("name", pointOfInterest.getName());
    featureObjectDTO.setProperties(properties);
  }

  private void extractOtherData(PointOfInterest pointOfInterest,
      FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(pointOfInterest.getIdentifier());
  }

  public abstract PointOfInterestDTO entityToDTO(PointOfInterest entity);

}

package com.fcul.adro.indoorNavigationSystem.repository;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FloorRepository extends JpaRepository<Floor, UUID> {

  @Query("select f from FLOORS f where f.building.id = :buildingId")
  List<Floor> findFloorsByBuildingId(@Param("buildingId") UUID buildingId);

  @Query("select f from FLOORS f where f.building.id = :buildingId and f.identifier = :id")
  Optional<Floor> findFloorByBuildingIdAndIdentifier(@Param("buildingId") UUID buildingId,
      @Param("id") UUID id);
}

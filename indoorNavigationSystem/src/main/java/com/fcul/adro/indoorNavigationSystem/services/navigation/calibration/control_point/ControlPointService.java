package com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ControlPointService {

  List<ControlPoint> fetchControlPoints();

  UUID createControlPoint(ControlPoint controlPoint);

  ControlPoint fetchControlPointById(UUID id);

  /**
   * @param latitude the latitude to look up
   * @param longitude the longitude to look uo
   * @return an object to check if there was a result based on the provided parameters
   */
  Optional<ControlPoint> fetchControlPointByCoordinates(Double latitude, Double longitude);

  /**
   * @param latitude the latitude to look up
   * @param longitude the longitude to look uo
   * @param radius the radius in meter to search for the controlPoint
   * @return an object to check if there was a result based on the provided parameters
   */
  Optional<ControlPoint> fetchControlPointByCoordinates(
      Double latitude, Double longitude, Double radius);
}

package com.fcul.adro.indoorNavigationSystem.controllers.rest.geo_json.building.floor;

import com.fcul.adro.indoorNavigationSystem.controllers.rest.geo_json.building.BuildingController;
import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.FloorMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import com.fcul.adro.indoorNavigationSystem.services.building.BuildingService;
import com.fcul.adro.indoorNavigationSystem.services.building.BuildingServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.floor.FloorService;
import com.fcul.adro.indoorNavigationSystem.services.floor.FloorServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "geo-json/buildings/{buildingId}/floors")
@Api(tags = "GeoJSON")
public class FloorController {

  private static final Logger LOGGER = LoggerFactory.getLogger(BuildingController.class);
  private final FloorMapper floorMapper;
  private BuildingService buildingService;
  private final FloorService floorService;

  @Autowired
  public FloorController(FloorMapper floorMapper,
      @BuildingServiceBean BuildingService buildingService,
      @FloorServiceBean FloorService floorService) {
    this.floorMapper = floorMapper;
    this.buildingService = buildingService;
    this.floorService = floorService;
  }

  @GetMapping
  @ApiOperation("Retrieves all known floors from a building as a GeoJson representation.")
  public ResponseEntity getGeoJsonBuildings(@PathVariable("buildingId") UUID buildingId) {
    Building building = this.buildingService.fetchBuildingById(buildingId);
    List<Floor> floors = this.floorService.fetchFloors(building);
    List<FeatureObjectDTO> geoJsonFloors = floors.stream()
        .map(this.floorMapper::floorToGeoJson).collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonFloors);
  }

  @PostMapping
  @ApiOperation("Creates a floor in a building based on the provided GeoJson representation.")
  public ResponseEntity createGeoJsonBuilding(@PathVariable("buildingId") UUID buildingId,
      @RequestBody FeatureObjectDTO featureObjectDTORoom) {
    Floor floor = this.floorMapper.geoJsonToFloor(featureObjectDTORoom);
    Building building = this.buildingService.fetchBuildingById(buildingId);
    UUID floorId = this.floorService.createFloor(building, floor);
    return ResponseEntity.status(HttpStatus.CREATED).body(floorId);
  }

  @GetMapping(path = "/{floorId}")
  @ApiOperation("Retrieves a floor in a building based on the provided identifier as a GeoJson representation.")
  public ResponseEntity getGeoJsonBuilding(@PathVariable("buildingId") UUID buildingId,
      @PathVariable(name = "floorId") UUID floorId) {
    Building building = this.buildingService.fetchBuildingById(buildingId);
    Floor floor = this.floorService.fetchFloorById(building, floorId);
    FeatureObjectDTO geoJsonFloor = floorMapper.floorToGeoJson(floor);
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonFloor);
  }

  @PutMapping(path = "{floorId}")
  @ApiOperation("Updates a floor in a building with the provided identifier, based on the provided GeoJson representation.")
  public ResponseEntity updateGeoJsonBuilding(@PathVariable(name = "buildingId") UUID buildingId,
      @PathVariable(name = "floorId") UUID floorId,
      @RequestBody FeatureObjectDTO featureObjectDTORoom) {
    Floor floor = this.floorMapper.geoJsonToFloor(featureObjectDTORoom);
    Building building = this.buildingService.fetchBuildingById(buildingId);
    this.floorService.updateFloor(building, floorId, floor);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

  @DeleteMapping(path = "{floorId}")
  @ApiOperation("Deletes a floor in building based on the provided identifier.")
  public ResponseEntity deleteGeoJsonBuilding(@PathVariable(name = "buildingId") UUID buildingId,
      @PathVariable(name = "floorId") UUID floorId) {
    Building building = this.buildingService.fetchBuildingById(buildingId);
    this.floorService.deleteFloor(building, floorId);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

}

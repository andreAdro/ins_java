package com.fcul.adro.indoorNavigationSystem.services.floor;

import com.fcul.adro.indoorNavigationSystem.error.exception.ResourceNotFoundException;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import com.fcul.adro.indoorNavigationSystem.repository.FloorRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
@FloorServiceBean
public class FloorServiceImpl implements FloorService {

  private FloorRepository floorRepository;

  public FloorServiceImpl(FloorRepository floorRepository) {
    this.floorRepository = floorRepository;
  }

  @Override
  public List<Floor> fetchFloors(Building building) {
    return this.floorRepository.findFloorsByBuildingId(building.getIdentifier());
  }

  @Override
  public UUID createFloor(Building building, Floor floor) {
    floor.setBuilding(building);
    Floor persistedFloor = this.floorRepository.save(floor);
    return persistedFloor.getIdentifier();
  }

  @Override
  public Floor fetchFloorById(Building building, UUID id) {
    Optional<Floor> maybeFloor = this.floorRepository
        .findFloorByBuildingIdAndIdentifier(building.getIdentifier(), id);
    return maybeFloor.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public void updateFloor(Building building, UUID id, Floor floor) {
    Floor fetchedFloor = this.fetchFloorById(building, id);
    fetchedFloor.update(floor);
    this.floorRepository.save(fetchedFloor);
  }

  @Override
  public void deleteFloor(Building building, UUID id) {
    Floor fetchedFloor = this.fetchFloorById(building, id);
    this.floorRepository.delete(fetchedFloor);
  }
}

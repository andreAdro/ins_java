package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polygon;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "BUILDINGS")
@PrimaryKeyJoinColumn(name = "IDENTIFIER")
public class Building extends Polygon {

  private static final long serialVersionUID = -8358420541827886217L;

  @Column(name = "NAME")
  private String name;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "building", orphanRemoval = true)
  private Set<Floor> floors;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  @JoinTable(name = "BUILDINGS_POINTS_OF_INTEREST",
      joinColumns = @JoinColumn(name = "BUILDING_ID", referencedColumnName = "IDENTIFIER"),
      inverseJoinColumns = @JoinColumn(name = "POINT_OF_INTEREST_ID")
  )
  private Set<PointOfInterest> pointsOfInterest;

  public void update(Building building) {
    super.update(building);
    this.name = building.getName();
    this.floors = building.getFloors();
    this.pointsOfInterest = building.getPointsOfInterest();
  }
}

package com.fcul.adro.indoorNavigationSystem.model.dto.geo_json;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel
public class PolygonDTO extends FeatureObjectDTO {

}

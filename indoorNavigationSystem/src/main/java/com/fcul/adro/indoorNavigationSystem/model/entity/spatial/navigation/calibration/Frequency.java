package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration;

import com.fcul.adro.indoorNavigationSystem.model.entity.WINSEntity;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@Entity(name = "FREQUENCIES")
@Table(
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"CONTROL_POINT_ID", "ACCESS_POINT_ID", "TIME_FRAME"})
    })
public class Frequency extends WINSEntity<Frequency> {

  private static final long serialVersionUID = 5599264266433549204L;

  @Include
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "CONTROL_POINT_ID", nullable = false)
  private ControlPoint controlPoint;

  @Include
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "ACCESS_POINT_ID", nullable = false)
  private AccessPoint accessPoint;

  @Column(name = "RSSI")
  private int rssi;

  @Include
  @Getter(value = AccessLevel.NONE)
  @Setter(value = AccessLevel.NONE)
  @Column(name = "TIME_FRAME", updatable = false)
  private Instant timeFrame = Instant
      .parse(String.format("%s:00.000Z", Instant.now().toString().substring(0, 16)));

  @Transient
  private double measurement;

  @Override
  public void update(Frequency frequency) {
    throw new RuntimeException("Frequency is not updatable");
  }

  public void measure(int rssi) {
    Double timeDistortionPercentage = this.controlPoint.getTimeDistortion();
    this.measurement = Math.abs(Math.abs(this.rssi) - Math.abs(rssi));
    if (timeDistortionPercentage > 0) {
      this.measurement = timeDistortionPercentage * (measurement);
    }
  }
}

package com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum FloorType {

  @JsonProperty("Basement")
  BASEMENT,
  @JsonProperty("First")
  FIRST,
  @JsonProperty("Second")
  SECOND,
  @JsonProperty("Third")
  THIRD,
  @JsonProperty("Fourth")
  FOURTH,
  @JsonProperty("Fifth")
  FIFTH
}

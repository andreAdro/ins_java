package com.fcul.adro.indoorNavigationSystem.controllers.rest.geo_json.building.floor.room;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.RoomMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import com.fcul.adro.indoorNavigationSystem.services.building.BuildingService;
import com.fcul.adro.indoorNavigationSystem.services.building.BuildingServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.floor.FloorService;
import com.fcul.adro.indoorNavigationSystem.services.floor.FloorServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.room.RoomService;
import com.fcul.adro.indoorNavigationSystem.services.room.RoomServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "geo-json/buildings/{buildingId}/floors/{floorId}/rooms")
@Api(tags = "GeoJSON")
public class RoomController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RoomController.class);
  private final RoomMapper roomMapper;
  private final RoomService roomService;
  private final FloorService floorService;
  private final BuildingService buildingService;

  @Autowired
  public RoomController(RoomMapper roomMapper,
      @RoomServiceBean RoomService roomService,
      @FloorServiceBean FloorService floorService,
      @BuildingServiceBean BuildingService buildingService) {
    this.roomMapper = roomMapper;
    this.roomService = roomService;
    this.floorService = floorService;
    this.buildingService = buildingService;
  }

  @GetMapping
  @ApiOperation("Retrieves all known rooms as a GeoJson representation.")
  public ResponseEntity getGeoJsonRooms(@PathVariable("buildingId") UUID buildingId,
      @PathVariable("floorId") UUID floorId) {
    Building building = this.buildingService.fetchBuildingById(buildingId);
    Floor floor = this.floorService.fetchFloorById(building, floorId);
    List<Room> rooms = this.roomService.fetchRooms(floor);
    List<FeatureObjectDTO> geoJsonWalls = rooms
        .stream()
        .map(this.roomMapper::roomToGeoJson)
        .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonWalls);
  }

  @PostMapping
  @ApiOperation("Creates a room based on the provided GeoJson representation.")
  public ResponseEntity createGeoJsonRoom(@PathVariable("buildingId") UUID buildingId,
      @PathVariable("floorId") UUID floorId,
      @RequestBody FeatureObjectDTO featureObjectDTORoom) {
    Room room = this.roomMapper.geoJsonToRoom(featureObjectDTORoom);
    Building building = this.buildingService.fetchBuildingById(buildingId);
    Floor floor = this.floorService.fetchFloorById(building, floorId);
    UUID roomId = this.roomService.createRoom(floor, room);
    return ResponseEntity.status(HttpStatus.CREATED).body(roomId);
  }

  @GetMapping(path = "/{roomId}")
  @ApiOperation("Retrieves a room based on the provided identifier as a GeoJson representation.")
  public ResponseEntity getGeoJsonRoom(@PathVariable("buildingId") UUID buildingId,
      @PathVariable("floorId") UUID floorId,
      @PathVariable("roomId") UUID roomId) {
    Building building = this.buildingService.fetchBuildingById(buildingId);
    Floor floor = this.floorService.fetchFloorById(building, floorId);
    Room room = this.roomService.fetchRoomById(floor, roomId);
    FeatureObjectDTO geoJsonRoom = this.roomMapper.roomToGeoJson(room);
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonRoom);
  }

  @PutMapping(path = "{roomId}")
  @ApiOperation("Updates a room with the provided identifier, based on the provided GeoJson representation.")
  public ResponseEntity updateGeoJsonBuilding(@PathVariable("buildingId") UUID buildingId,
      @PathVariable("floorId") UUID floorId,
      @PathVariable(name = "roomId") UUID roomId,
      @RequestBody FeatureObjectDTO featureObjectDTORoom) {
    Room room = this.roomMapper.geoJsonToRoom(featureObjectDTORoom);
    Building building = this.buildingService.fetchBuildingById(buildingId);
    Floor floor = this.floorService.fetchFloorById(building, floorId);
    this.roomService.updateRoom(floor, roomId, room);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

  @DeleteMapping(path = "{roomId}")
  @ApiOperation("Deletes a room based on the provided identifier.")
  public ResponseEntity deleteGeoJsonBuilding(@PathVariable("buildingId") UUID buildingId,
      @PathVariable("floorId") UUID floorId,
      @PathVariable(name = "roomId") UUID roomId) {
    Building building = this.buildingService.fetchBuildingById(buildingId);
    Floor floor = this.floorService.fetchFloorById(building, floorId);
    this.roomService.deleteRoom(floor, roomId);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

}

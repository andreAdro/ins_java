package com.fcul.adro.indoorNavigationSystem.repository.control_point;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

public class ControlPointCustomRepoImpl implements ControlPointCustomRepo {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<ControlPoint> getControlPointsByDistance(double latitude, double longitude,
      double range) {
    StoredProcedureQuery findControlPointsByDistance =
        entityManager.createNamedStoredProcedureQuery("controlPointsGeoDistance");
    findControlPointsByDistance.setParameter("sourceLatitude", latitude);
    findControlPointsByDistance.setParameter("sourceLongitude", longitude);
    findControlPointsByDistance.setParameter("bufferSize", range);
    return findControlPointsByDistance.getResultList();
  }
}

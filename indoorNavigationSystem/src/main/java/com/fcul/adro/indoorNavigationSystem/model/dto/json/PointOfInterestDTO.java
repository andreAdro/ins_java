package com.fcul.adro.indoorNavigationSystem.model.dto.json;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PointOfInterestDTO extends WINSDTO {

  private static final long serialVersionUID = -201378892625993636L;

  private String name;
}

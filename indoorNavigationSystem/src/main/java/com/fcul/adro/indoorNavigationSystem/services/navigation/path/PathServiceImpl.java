package com.fcul.adro.indoorNavigationSystem.services.navigation.path;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.services.navigation.path.algorithms.PathFinderService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.path.algorithms.dijkstra.DjikstraServiceBean;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@PathServiceBean
public class PathServiceImpl implements PathService {

  private PathFinderService pathFinderService;

  @Autowired
  public PathServiceImpl(@DjikstraServiceBean PathFinderService pathFinderService) {
    this.pathFinderService = pathFinderService;
  }

  @Override
  public List<Point> obtainPath(ControlPoint controlPoint, ControlPoint pointOfInterest) {
    return pathFinderService.getPathAsPoints(controlPoint, pointOfInterest);
  }
}

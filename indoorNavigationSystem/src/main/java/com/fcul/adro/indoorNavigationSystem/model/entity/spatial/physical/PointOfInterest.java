package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "POINTS_OF_INTEREST")
@PrimaryKeyJoinColumn(name = "POINT_ID")
public class PointOfInterest extends Point {

  private static final long serialVersionUID = 2283369597697708645L;

  @Column(name = "NAME")
  private String name;

  public void update(PointOfInterest point) {
    super.update(point);
    this.name = point.getName();
  }
}

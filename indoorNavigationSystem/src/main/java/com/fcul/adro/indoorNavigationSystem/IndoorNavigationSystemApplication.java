package com.fcul.adro.indoorNavigationSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class IndoorNavigationSystemApplication extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(IndoorNavigationSystemApplication.class, args);
  }
}

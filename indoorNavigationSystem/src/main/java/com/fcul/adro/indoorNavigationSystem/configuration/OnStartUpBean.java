package com.fcul.adro.indoorNavigationSystem.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fcul.adro.indoorNavigationSystem.mappers.PointOfInterestMapper;
import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.BuildingMapper;
import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.FloorMapper;
import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.RoomMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeaturesCollectionDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.PointDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Floor;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import com.fcul.adro.indoorNavigationSystem.services.building.BuildingService;
import com.fcul.adro.indoorNavigationSystem.services.floor.FloorService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point.ControlPointService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.edge.EdgeService;
import com.fcul.adro.indoorNavigationSystem.services.room.RoomService;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OnStartUpBean {
  private static final Logger LOGGER = LoggerFactory.getLogger(OnStartUpBean.class);
  private final PointOfInterestMapper pointOfInterestMapper;
  private final RoomMapper roomMapper;
  private final RoomService roomService;
  private final FloorMapper floorMapper;
  private final FloorService floorService;
  private final BuildingMapper buildingMapper;
  private final BuildingService buildingService;
  private final ControlPointService controlPointService;
  private final EdgeService edgeService;

  @Autowired
  public OnStartUpBean(
      PointOfInterestMapper pointOfInterestMapper,
      RoomMapper roomMapper,
      RoomService roomService,
      FloorMapper floorMapper,
      FloorService floorService,
      BuildingMapper buildingMapper,
      BuildingService buildingService,
      ControlPointService controlPointService,
      EdgeService edgeService) {
    this.pointOfInterestMapper = pointOfInterestMapper;
    this.roomMapper = roomMapper;
    this.roomService = roomService;
    this.floorMapper = floorMapper;
    this.floorService = floorService;
    this.buildingMapper = buildingMapper;
    this.buildingService = buildingService;
    this.controlPointService = controlPointService;
    this.edgeService = edgeService;
  }

  @PostConstruct
  public void onInit() {
    if (CollectionUtils.isEmpty(this.buildingService.fetchBuildings())) {
      this.loadData();
    }
  }

  private void loadData() {
    loadC8PathEntities();
    loadC8BuildingEntities();
  }

  private void loadC8PathEntities() {
    String pathGeoJSONFile = "/floorOnePath.json";
    FeaturesCollectionDTO paths = extractFileContent(pathGeoJSONFile);
    FeatureObjectDTO featureObjectDTO = paths.getFeatures().get(0);
    List<List<PointDTO>> lineCoordinates = featureObjectDTO.getGeometry().getPolygonCoordinates();
    int iter = 0;
    for (List<PointDTO> lineCoordinate : lineCoordinates) {
      iter += 1;
      for (int i = 0; i < lineCoordinate.size(); i++) {
        if (i == 0) {
          continue;
        }
        PointDTO previousPointDTO = lineCoordinate.get(i - 1);
        Optional<ControlPoint> previousControlPoint =
            fetchControlPointBasedOnCoordinates(previousPointDTO);
        if (!previousControlPoint.isPresent()) {
          previousControlPoint = createControlPoint(iter, i, previousPointDTO);
        }
        PointDTO currentPointDTO = lineCoordinate.get(i);
        Optional<ControlPoint> currentControlPoint =
            fetchControlPointBasedOnCoordinates(currentPointDTO);
        if (!currentControlPoint.isPresent()) {
          currentControlPoint = createControlPoint(iter, i, currentPointDTO);
        }
        // creates bidirectional paths
        this.edgeService.createEdgeFromControlPoints(
            previousControlPoint.get(), currentControlPoint.get());
        this.edgeService.createEdgeFromControlPoints(
            currentControlPoint.get(), previousControlPoint.get());
      }
    }
  }

  private Optional<ControlPoint> createControlPoint(int iter, int i, PointDTO pointDTO) {
    ControlPoint controlPoint = new ControlPoint();
    controlPoint.setName(
        String.format("%s | %s   ->    %s", iter, i, UUID.randomUUID().toString()));
    controlPoint.setAltitude(pointDTO.getAltitude());
    controlPoint.setLatitude(pointDTO.getLatitude());
    controlPoint.setLongitude(pointDTO.getLongitude());
    UUID controlPointId = this.controlPointService.createControlPoint(controlPoint);
    return Optional.of(this.controlPointService.fetchControlPointById(controlPointId));
  }

  private Optional<ControlPoint> fetchControlPointBasedOnCoordinates(PointDTO previousPoint) {
    return this.controlPointService.fetchControlPointByCoordinates(
        previousPoint.getLatitude(), previousPoint.getLongitude());
  }

  private void loadC8BuildingEntities() {
    String buildingsGeoJSONFile = "/building.json";
    FeaturesCollectionDTO buildings = extractFileContent(buildingsGeoJSONFile);
    Building building = this.buildingMapper.geoJsonToBuilding(buildings.getFeatures().get(0));
    this.buildingService.createBuilding(building);

    String floorsGeoJSONFile = "/floorOne.json"; // Change arcGIS name for field name for type
    FeaturesCollectionDTO floors = extractFileContent(floorsGeoJSONFile);
    Floor floor = this.floorMapper.geoJsonToFloor(floors.getFeatures().get(0));
    this.floorService.createFloor(building, floor);

    String roomsGeoJSONFile = "/floorOneRooms.json";
    FeaturesCollectionDTO rooms = extractFileContent(roomsGeoJSONFile);

    String doorsGeoJSONFile = "/floorOneDoors.json";
    FeaturesCollectionDTO doors = extractFileContent(doorsGeoJSONFile);

    List<Room> roomsWithDoors = mergeToRooms(rooms.getFeatures(), doors.getFeatures());
    roomsWithDoors.forEach(room -> this.roomService.createRoom(floor, room));
  }

  private FeaturesCollectionDTO extractFileContent(String filePathName) {
    try (InputStream inputStream = getClass().getResourceAsStream(filePathName)) {
      String parsedFileString = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
      ObjectMapper mapper = new ObjectMapper();
      return mapper.readValue(parsedFileString, FeaturesCollectionDTO.class);
    } catch (IOException e) {
      LOGGER.error("Unable to parse files.. Exception []", e);
      throw new RuntimeException(String.format("Unable to parse JSON contents. Error [%s]", e));
    }
  }

  private List<Room> mergeToRooms(
      List<FeatureObjectDTO> target, List<FeatureObjectDTO> pointsOfInterest) {
    HashMap<String, PointOfInterest> pointsOfInterestByName =
        extractPointsOfInterestByName(pointsOfInterest);
    return target.stream()
        .map(this.roomMapper::geoJsonToRoom)
        .peek(
            room -> {
              PointOfInterest pointOfInterest = pointsOfInterestByName.get(room.getName());
              Set<PointOfInterest> roomPointsOfInterest = new LinkedHashSet<>();
              roomPointsOfInterest.add(pointOfInterest);
              room.setPointsOfInterest(roomPointsOfInterest);
            })
        .collect(Collectors.toList());
  }

  private HashMap<String, PointOfInterest> extractPointsOfInterestByName(
      List<FeatureObjectDTO> pointsOfInterest) {
    HashMap<String, PointOfInterest> pointsOfInterestByName = new HashMap<>();
    pointsOfInterest.forEach(
        featureObjectDTO -> {
          PointOfInterest pointOfInterest =
              this.pointOfInterestMapper.geoJsonToPointOfInterest(featureObjectDTO);
          pointsOfInterestByName.put(pointOfInterest.getName(), pointOfInterest);
        });
    return pointsOfInterestByName;
  }
}

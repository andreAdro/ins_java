package com.fcul.adro.indoorNavigationSystem.controllers.rest.geo_json.building;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.BuildingMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.services.building.BuildingService;
import com.fcul.adro.indoorNavigationSystem.services.building.BuildingServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "geo-json/buildings")
@Api(tags = "GeoJSON")
public class BuildingController {

  private static final Logger LOGGER = LoggerFactory.getLogger(BuildingController.class);
  private final BuildingMapper buildingMapper;
  private final BuildingService buildingService;

  @Autowired
  public BuildingController(BuildingMapper buildingMapper,
      @BuildingServiceBean BuildingService buildingService) {
    this.buildingMapper = buildingMapper;
    this.buildingService = buildingService;
  }


  @GetMapping
  @ApiOperation("Retrieves all known buildings as a GeoJson representation.")
  public ResponseEntity getGeoJsonBuildings() {
    List<Building> buildings = this.buildingService.fetchBuildings();
    List<FeatureObjectDTO> geoJsonWalls = buildings
        .stream()
        .map(this.buildingMapper::buildingToGeoJson)
        .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonWalls);
  }

  @PostMapping
  @ApiOperation("Creates a building based on the provided GeoJson representation.")
  public ResponseEntity createGeoJsonBuilding(@RequestBody FeatureObjectDTO featureObjectDTORoom) {
    Building building = this.buildingMapper.geoJsonToBuilding(featureObjectDTORoom);
    UUID buildingId = this.buildingService.createBuilding(building);
    return ResponseEntity.status(HttpStatus.CREATED).body(buildingId);
  }

  @GetMapping(path = "/{buildingId}")
  @ApiOperation("Retrieves a building based on the provided identifier as a GeoJson representation.")
  public ResponseEntity getGeoJsonBuilding(@PathVariable("buildingId") UUID id) {
    Building building = this.buildingService.fetchBuildingById(id);
    FeatureObjectDTO geoJsonBuilding = this.buildingMapper.buildingToGeoJson(building);
    return ResponseEntity.status(HttpStatus.OK).body(geoJsonBuilding);
  }

  @PutMapping(path = "{buildingId}")
  @ApiOperation("Updates a building with the provided identifier, based on the provided GeoJson representation.")
  public ResponseEntity updateGeoJsonBuilding(@PathVariable(name = "buildingId") UUID id,
      @RequestBody FeatureObjectDTO featureObjectDTORoom) {
    Building building = this.buildingMapper.geoJsonToBuilding(featureObjectDTORoom);
    this.buildingService.updateBuilding(id, building);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

  @DeleteMapping(path = "{buildingId}")
  @ApiOperation("Deletes a building based on the provided identifier.")
  public ResponseEntity deleteGeoJsonBuilding(@PathVariable(name = "buildingId") UUID id) {
    this.buildingService.deleteBuilding(id);
    return ResponseEntity.status(HttpStatus.ACCEPTED).build();
  }

}

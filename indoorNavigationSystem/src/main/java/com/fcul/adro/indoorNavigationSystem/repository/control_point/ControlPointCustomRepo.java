package com.fcul.adro.indoorNavigationSystem.repository.control_point;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.util.List;
import org.springframework.data.repository.query.Param;

public interface ControlPointCustomRepo {

  /**
   * returns control points  within the requested range of the provided latitude and longitude the
   * points will be ordered by close to further
   *
   * @param latitude the latitude to lookpup points
   * @param longitude the longitude to lookpup points
   * @param range the range to lookpup points
   * @return a collection of control points
   */
  List<ControlPoint> getControlPointsByDistance(
      @Param("sourceLatitude") double latitude,
      @Param("sourceLongitude") double longitude,
      @Param("bufferSize") double range
  );
}

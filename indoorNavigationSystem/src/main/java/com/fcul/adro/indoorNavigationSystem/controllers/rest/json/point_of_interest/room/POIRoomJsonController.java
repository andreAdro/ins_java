package com.fcul.adro.indoorNavigationSystem.controllers.rest.json.point_of_interest.room;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.RoomMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.json.RoomDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Room;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestService;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.PointOfInterestServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.room.POIRoomServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.point_of_interest.room.RoomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/points-of-interest/{pointOfInterestId}/rooms")
@Api(tags = "JSON")
public class POIRoomJsonController {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      POIRoomJsonController.class);
  private final PointOfInterestService pointOfInterestService;
  private final RoomService roomService;
  private RoomMapper roomMapper;

  @Autowired
  public POIRoomJsonController(
      @PointOfInterestServiceBean PointOfInterestService pointOfInterestService,
      @POIRoomServiceBean RoomService roomService,
      RoomMapper roomMapper) {
    this.pointOfInterestService = pointOfInterestService;
    this.roomService = roomService;
    this.roomMapper = roomMapper;
  }

  @GetMapping
  @ApiOperation("Retrieves all known points of interest as a json representation.")
  public ResponseEntity getGeoJsonPointsOfInterest(
      @PathVariable("pointOfInterestId") UUID pointOfInterestId) {
    PointOfInterest pointOfInterest = this.pointOfInterestService
        .fetchPointOfInterestById(pointOfInterestId);
    List<Room> rooms = this.roomService.fetchRooms(pointOfInterest);
    List<RoomDTO> roomsDTO = rooms.stream()
        .map(room -> this.roomMapper.entityToDTO(room))
        .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(roomsDTO);
  }
}

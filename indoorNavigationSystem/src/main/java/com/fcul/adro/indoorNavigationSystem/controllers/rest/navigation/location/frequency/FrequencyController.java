package com.fcul.adro.indoorNavigationSystem.controllers.rest.navigation.location.frequency;

import com.fcul.adro.indoorNavigationSystem.error.exception.ResourceNotFoundException;
import com.fcul.adro.indoorNavigationSystem.mappers.FrequencyMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.json.FrequencyDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.AccessPoint;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.calibration.Frequency;
import com.fcul.adro.indoorNavigationSystem.services.calibration.AccessPointService;
import com.fcul.adro.indoorNavigationSystem.services.calibration.AccessPointServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.calibration.FrequencyService;
import com.fcul.adro.indoorNavigationSystem.services.calibration.FrequencyServiceBean;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point.ControlPointService;
import com.fcul.adro.indoorNavigationSystem.services.navigation.calibration.control_point.ControlPointServiceBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("navigation/control-points/{controlPointId}/frequencies")
@Api(tags = "Navigation")
public class FrequencyController {

  private FrequencyMapper frequencyMapper;
  private ControlPointService controlPointService;
  private FrequencyService frequencyService;
  private AccessPointService accessPointService;

  public FrequencyController(
      FrequencyMapper frequencyMapper,
      @ControlPointServiceBean ControlPointService controlPointService,
      @FrequencyServiceBean FrequencyService frequencyService,
      @AccessPointServiceBean AccessPointService accessPointService) {
    this.frequencyMapper = frequencyMapper;
    this.controlPointService = controlPointService;
    this.frequencyService = frequencyService;
    this.accessPointService = accessPointService;
  }

  @GetMapping
  @ApiOperation("Retrieves all known frequencies.")
  public ResponseEntity getFrequencies(@PathVariable("controlPointId") UUID controlPointId) {
    ControlPoint fetchedControlPoint =
        this.controlPointService.fetchControlPointById(controlPointId);
    List<Frequency> fetchedFrequencies =
        this.frequencyService.fetchFrequencies(fetchedControlPoint);
    if (CollectionUtils.isEmpty(fetchedFrequencies)) {
      return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
    }
    List<FrequencyDTO> frequencies =
        fetchedFrequencies.stream()
            .map(frequency -> this.frequencyMapper.frequencyToDTO(frequency))
            .collect(Collectors.toList());
    return ResponseEntity.status(HttpStatus.OK).body(frequencies);
  }

  @PostMapping
  @ApiOperation("Creates a frequency based on the provided representation.")
  public ResponseEntity createFrequency(
      @PathVariable("controlPointId") UUID controlPointId, @RequestBody FrequencyDTO frequencyDTO) {
    ControlPoint fetchedControlPoint =
        this.controlPointService.fetchControlPointById(controlPointId);
    AccessPoint accessPoint = this.fetchCreatedAccessPoint(frequencyDTO);
    Frequency frequency = this.frequencyMapper.dtoToFrequency(frequencyDTO);
    UUID frequencyId =
        this.frequencyService.createFrequency(fetchedControlPoint, accessPoint, frequency);
    return ResponseEntity.status(HttpStatus.CREATED).body(frequencyId);
  }

  private AccessPoint fetchCreatedAccessPoint(FrequencyDTO frequency) {
    try {
      return this.accessPointService.fetchAccessPointByBSSID(frequency.getBssid());
    } catch (ResourceNotFoundException exception) {
      AccessPoint accessPoint = this.frequencyMapper.dtoToAccessPoint(frequency);
      this.accessPointService.createAccessPoint(accessPoint);
      return this.accessPointService.fetchAccessPointByBSSID(frequency.getBssid());
    }
  }
}

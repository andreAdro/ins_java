package com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry;

import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.PointDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.GeometryType;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polygon;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polyline;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class PolygonMapper extends PolylineMapper {

  protected Polygon geoJsonToPolygon(List<List<PointDTO>> polylineSegments, GeometryType type) {
    List<Polyline> polylines = new LinkedList<>();
    for (List<PointDTO> pointsSegment : polylineSegments) {
      Polyline polyline = super.geoJsonToPolyline(pointsSegment, GeometryType.LINE_STRING);
      polylines.add(polyline);
    }
    Polygon polygon = new Polygon();
    polygon.setPolylines(polylines);
    polygon.setGeometryType(type);
    return polygon;
  }

  protected List<List<Double[]>> polygonToGeoJsonCoordinates(Polygon polygon) {
    List<List<Double[]>> parsedCoordinates = new ArrayList<>();
    for (Polyline polyline : polygon.getPolylines()) {
      List<Double[]> coordinates = super.polylineToGeoJsonCoordinates(polyline);
      parsedCoordinates.add(coordinates);
    }
    return parsedCoordinates;
  }

}

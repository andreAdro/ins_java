package com.fcul.adro.indoorNavigationSystem.model.dto.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;
import lombok.Data;

@Data
public class WINSDTO implements Serializable {

  private static final long serialVersionUID = 3474907275719292944L;

  @JsonProperty(access = Access.READ_ONLY)
  private UUID identifier;
  @JsonProperty(access = Access.READ_ONLY)
  private Instant updatedDate;
  @JsonProperty(access = Access.READ_ONLY)
  private Instant createdDate;

}

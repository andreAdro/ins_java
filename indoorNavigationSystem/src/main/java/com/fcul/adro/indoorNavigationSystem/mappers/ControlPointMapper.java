package com.fcul.adro.indoorNavigationSystem.mappers;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry.PointMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.GeometryDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.GeometryType;
import com.fcul.adro.indoorNavigationSystem.model.dto.json.ControlPointDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.util.HashMap;
import java.util.Map;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public abstract class ControlPointMapper extends PointMapper {

  public ControlPoint geoJsonToControlPoint(FeatureObjectDTO featureObjectDTO) {
    ControlPoint controlPoint = new ControlPoint();
    appendCoordinate(controlPoint, featureObjectDTO);
    appendProperties(controlPoint, featureObjectDTO);
    return controlPoint;
  }

  private void appendCoordinate(ControlPoint controlPoint,
      FeatureObjectDTO featureObjectDTO) {
    Point point = super.geoJsonCoordinateToPoint(
        featureObjectDTO.getGeometry().getPointCoordinates()
    );
    controlPoint.update(point);
  }

  private void appendProperties(ControlPoint controlPoint,
      FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = featureObjectDTO.getProperties();
    Object name = properties.get("name");
    if (name instanceof String) {
      controlPoint.setName((String) name);
    }
  }

  public FeatureObjectDTO controlPointToGeoJson(ControlPoint pointOfInterest) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinate(pointOfInterest, featureObjectDTO);
    extractProperties(pointOfInterest, featureObjectDTO);
    extractOtherData(pointOfInterest, featureObjectDTO);
    return featureObjectDTO;
  }

  private void extractCoordinate(ControlPoint controlPoint,
      FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = super.pointToGeoJsonCoordinates(controlPoint);
    featureObjectDTO.getGeometry().setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry().setType(GeometryType.POINT);
  }

  private void extractProperties(ControlPoint controlPoint,
      FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = new HashMap<>();
    properties.put("name", controlPoint.getName());
    featureObjectDTO.setProperties(properties);
  }

  private void extractOtherData(ControlPoint controlPoint,
      FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(controlPoint.getIdentifier());
  }

  public abstract ControlPointDTO entityToDTO(ControlPoint entity);

}

package com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry;

import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.PointDTO;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;

public abstract class PointMapper {

  protected Point geoJsonCoordinateToPoint(PointDTO pointCoordinates) {
    Point point = new Point();
    point.setAltitude(pointCoordinates.getAltitude());
    point.setLongitude(pointCoordinates.getLongitude());
    point.setLatitude(pointCoordinates.getLatitude());
    return point;
  }

  protected Double[] pointToGeoJsonCoordinates(Point point) {
    return point.getAltitude() == null
        ? new Double[]{point.getLongitude(), point.getLatitude()}
        : new Double[]{point.getLongitude(), point.getLatitude(), point.getAltitude()};
  }

}

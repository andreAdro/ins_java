package com.fcul.adro.indoorNavigationSystem.services.building;

import com.fcul.adro.indoorNavigationSystem.error.exception.ResourceNotFoundException;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.repository.BuildingRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
@BuildingServiceBean
public class BuildingServiceImpl implements BuildingService {

  private BuildingRepository buildingRepository;

  public BuildingServiceImpl(BuildingRepository buildingRepository) {
    this.buildingRepository = buildingRepository;
  }

  @Override
  public List<Building> fetchBuildings() {
    return this.buildingRepository.findAll();
  }

  @Override
  public UUID createBuilding(Building building) {
    Building persistedBuilding = this.buildingRepository.save(building);
    return persistedBuilding.getIdentifier();
  }

  @Override
  public Building fetchBuildingById(UUID id) {
    Optional<Building> maybeBuilding = this.buildingRepository.findById(id);
    return maybeBuilding.orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public void updateBuilding(UUID id, Building building) {
    Building fetchedBuilding = this.fetchBuildingById(id);
    fetchedBuilding.update(building);
  }

  @Override
  public void deleteBuilding(UUID id) {
    this.buildingRepository.deleteById(id);
  }
}

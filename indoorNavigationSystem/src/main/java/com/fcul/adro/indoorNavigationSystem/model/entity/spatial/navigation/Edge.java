package com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polyline;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@Entity(name = "EDGES")
@PrimaryKeyJoinColumn(name = "POLYLINE_ID")
public class Edge extends Polyline {

  private static final long serialVersionUID = -3928971543958561783L;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name = "START_POINT_ID", nullable = false)
  private ControlPoint start;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name = "END_POINT_ID", nullable = false)
  private ControlPoint end;

  @Column(name = "DISTANCE")
  private Double distance;

  public Edge(ControlPoint start, ControlPoint end) {
    super();
    this.start = start;
    this.end = end;
    this.distance = calculateDistance(start, end);
  }

  private double calculateDistance(ControlPoint start, ControlPoint end) {
    return calculateDistanceInMeters(start.getLatitude(),
        end.getLatitude(),
        start.getLongitude(),
        end.getLongitude(),
        start.getAltitude(),
        end.getAltitude());
  }

  /**
   * from: https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude
   *
   * Calculate distance between two points in latitude and longitude taking into account height
   * difference. If you are not interested in height difference pass 0.0. Uses Haversine method as
   * its base.
   *
   * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters el2 End altitude in
   * meters
   *
   * @returns Distance in Meters
   */
  private double calculateDistanceInMeters(double lat1, double lat2, double lon1,
      double lon2, double el1, double el2) {

    final int R = 6371; // Radius of the earth

    double latDistance = Math.toRadians(lat2 - lat1);
    double lonDistance = Math.toRadians(lon2 - lon1);
    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
        + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
        * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = R * c * 1000; // convert to meters

    double height = el1 - el2;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    return Math.sqrt(distance);
  }

}

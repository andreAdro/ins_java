package com.fcul.adro.indoorNavigationSystem.services.navigation.path.algorithms;

import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Point;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.ControlPoint;
import java.util.List;

public interface PathFinderService {

  List<Point> getPathAsPoints(ControlPoint start, ControlPoint goal);
}

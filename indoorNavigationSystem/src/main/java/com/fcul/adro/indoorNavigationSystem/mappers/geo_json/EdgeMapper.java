package com.fcul.adro.indoorNavigationSystem.mappers.geo_json;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry.PolylineMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.GeometryDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polyline;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.navigation.Edge;
import java.util.ArrayList;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public abstract class EdgeMapper extends PolylineMapper {

  public FeatureObjectDTO edgeToGeoJson(Edge edge) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinates(edge, featureObjectDTO);
    extractOtherData(edge, featureObjectDTO);
    return featureObjectDTO;
  }

  private void extractCoordinates(Edge edge, FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = edgeToCoordinates(edge);
    featureObjectDTO.getGeometry().setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry().setType(edge.getGeometryType());
  }

  private List<Double[]> edgeToCoordinates(Edge edge) {
    List<Double[]> parsedCoordinates = new ArrayList<>();
    Double[] startCoordinate = super.pointToGeoJsonCoordinates(edge.getStart());
    Double[] endCoordinate = super.pointToGeoJsonCoordinates(edge.getEnd());
    parsedCoordinates.add(startCoordinate);
    parsedCoordinates.add(endCoordinate);
    return parsedCoordinates;
  }

  private void extractOtherData(Polyline polyline, FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(polyline.getIdentifier());
  }


}

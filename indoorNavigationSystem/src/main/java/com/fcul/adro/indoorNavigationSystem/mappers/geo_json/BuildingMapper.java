package com.fcul.adro.indoorNavigationSystem.mappers.geo_json;

import com.fcul.adro.indoorNavigationSystem.mappers.geo_json.geometry.PolygonMapper;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.FeatureObjectDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json.GeometryDTO;
import com.fcul.adro.indoorNavigationSystem.model.dto.geo_json._enum.FeatureType;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.geometry.Polygon;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.Building;
import com.fcul.adro.indoorNavigationSystem.model.entity.spatial.physical.PointOfInterest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Mapper
@Component
public abstract class BuildingMapper extends PolygonMapper {

  @Autowired
  protected PropertiesMapper propertiesMapper;

  /**
   *
   */
  public Building geoJsonToBuilding(FeatureObjectDTO featureObjectDTO) {
    Building building = new Building();
    appendCoordinates(building, featureObjectDTO);
    appendProperties(building, featureObjectDTO);
    return building;
  }

  private void appendCoordinates(Building building, FeatureObjectDTO featureObjectDTO) {
    Polygon polygon = super.geoJsonToPolygon(
        featureObjectDTO.getGeometry().getPolygonCoordinates(),
        featureObjectDTO.getGeometry().getType()
    );
    building.update(polygon);
  }

  private void appendProperties(Building building, FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = featureObjectDTO.getProperties();
    appendName(building, properties);
    appendPointsOfInterest(building, properties);
  }

  private void appendName(Building building, Map<String, Object> properties) {
    Object roomName = properties.get("name");
    if (roomName instanceof String) {
      building.setName((String) roomName);
    }
  }

  private void appendPointsOfInterest(Building building, Map<String, Object> properties) {
    Object pointsOfInterestArray = properties.get("pointsOfInterest");
    Set<PointOfInterest> pointOfInterests =
        this.propertiesMapper.propertiesToPointsOfInterest(pointsOfInterestArray);
    building.setPointsOfInterest(pointOfInterests);
  }

  /**
   *
   */
  public FeatureObjectDTO buildingToGeoJson(Building building) {
    FeatureObjectDTO featureObjectDTO = new FeatureObjectDTO();
    extractCoordinates(building, featureObjectDTO);
    extractProperties(building, featureObjectDTO);
    extractOtherData(building, featureObjectDTO);
    return featureObjectDTO;
  }

  private void extractCoordinates(Building building, FeatureObjectDTO featureObjectDTO) {
    if (null == featureObjectDTO.getGeometry()) {
      featureObjectDTO.setGeometry(new GeometryDTO());
    }
    Object parsedCoordinates = super.polygonToGeoJsonCoordinates(building);
    featureObjectDTO.getGeometry().setCoordinates(parsedCoordinates);
    featureObjectDTO.getGeometry().setType(building.getGeometryType());
  }


  private void extractProperties(Building building, FeatureObjectDTO featureObjectDTO) {
    Map<String, Object> properties = new HashMap<>();
    extractName(building, properties);
    extractPointsOfInterest(building, properties);
    featureObjectDTO.setProperties(properties);
  }

  private void extractName(Building building, Map<String, Object> properties) {
    properties.put("name", building.getName());
  }

  private void extractPointsOfInterest(Building building, Map<String, Object> properties) {
    this.propertiesMapper
        .extractPointsOfInterestToProperties(building.getPointsOfInterest(), properties);
  }

  private void extractOtherData(Building building, FeatureObjectDTO featureObjectDTO) {
    featureObjectDTO.setType(FeatureType.FEATURE);
    featureObjectDTO.setId(building.getIdentifier());
  }
}

DROP PROCEDURE IF EXISTS controlPointsGeoDistance^;
CREATE PROCEDURE controlPointsGeoDistance(IN sourceLatitude double,
                                          IN sourceLongitude double,
                                          IN bufferSize double)
BEGIN
    SELECT p.*,
           cp.*,
           (
                   6371000 *
                   acos(cos(radians(sourceLatitude)) *
                        cos(radians(latitude)) *
                        cos(radians(longitude) -
                            radians(sourceLongitude)) +
                        sin(radians(sourceLatitude)) *
                        sin(radians(latitude)))
               ) AS distance
    FROM points as p
             inner join control_points cp
                        on p.identifier = cp.point_id
    HAVING distance <= bufferSize
    ORDER BY distance;
END ^;